
import java.util.*;

public class OX {

    private static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private static void showTable(Character table[][]) {
        System.out.println("  1 2 3");
        for (int i = 0; i < 3; i++) {
            System.out.print((i + 1));
            for (int j = 0; j < 3; j++) {
                System.out.print(" " + table[i][j]);
            }
            System.out.println("");
        }
    }

    private static void showTurn(Character player) {
        System.out.println(player + " turn");
    }

    private static void inputRowCol(Character table[][], Character player) {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input Row Col: ");
        try {
            int row = kb.nextInt();
            int col = kb.nextInt();
            setTable(table, player, row, col);
        } catch (Exception e) {
            showErrorInput(table, player);
        }
    }

    private static void setTable(Character table[][], Character player, int row, int col) {
        if (table[row - 1][col - 1] == ('-')) {
            table[row - 1][col - 1] = player;
        } else {
            showErrorInput(table, player);
        }
    }

    private static void showErrorInput(Character table[][], Character player) {
        System.out.println("Error input again!!");
        inputRowCol(table, player);
    }

    private static boolean checkWin(Character table[][], Character player) {
        for (int i = 0; i < 3; i++) {
            if (checkRow(table, i, player)) {
                return true;
            } else if (checkCol(table, i, player)) {
                return true;
            } else if (checkX1(table, player)) {
                return true;
            } else if (checkX2(table, player)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(Character table[][], int i, Character player) {
        for (int r = 0; r < 3; r++) {
            if (table[i][r] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol(Character table[][], int i, Character player) {
        for (int c = 0; c < 3; c++) {
            if (table[c][i] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1(Character table[][], Character player) {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2(Character table[][], Character player) {
        for (int i = 0; i < 3; i++) {
            if (table[2 - i][0 + i] != player) {
                return false;
            }
        }
        return true;
    }

    private static char switchPlayer(Character player) {
        player = player == 'O' ? 'X' : 'O';
        return player;
    }

    private static void showWin(Character table[][], int turn, Character player) {
        showTable(table);
        if (turn != 9) {
            System.out.println("Player " + player + " Win");
        } else {
            System.out.println("Draw");
        }

    }

    private static void showBye() {
        System.out.println("Bye bye.");
    }

    public static void main(String[] args) {
        Character[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char player = 'O';
        int turn = 0;
        showWelcome();
        while (turn != 9) {
            showTable(table);
            showTurn(player);
            inputRowCol(table, player);
            turn++;
            if (checkWin(table, player)) {
                break;
            }
            player = switchPlayer(player);
        }
        showWin(table, turn, player);
        showBye();
    }

}
